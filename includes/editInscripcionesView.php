  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Editar inscripción</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inscripciones</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Inscripciones</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php
# capturo de los query params de otra forma porque la anterior no funcioanaba
#global $materias_id_from_queryparam;
                $materias_id_from_queryparam = htmlspecialchars($_GET["materias_id"]);
#global $usuarios_id_from_queryparam;                
                $usuarios_id_from_queryparam = htmlspecialchars($_GET["usuarios_id"]);

                try {

                    $stmt = "SELECT usuarios_id,
                    usuarios.nombre as nombre_usuario,
                    materias_id,
                    materias.nombre as nombre_materia
                    FROM usuarios_has_materias
                    INNER JOIN materias ON usuarios_has_materias.materias_id=$materias_id_from_queryparam
                    INNER JOIN usuarios ON usuarios_has_materias.usuarios_id=$usuarios_id_from_queryparam
                    WHERE materias_id = $materias_id_from_queryparam ;";

                    $resultado = $conn->query($stmt);
                    
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    echo $error;
                }
                $inscripcion = $resultado->fetch_assoc();    
              ?>
            
              <form action="includes/models/inscripcionesModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputIdUsuario">ID Usuario</label>
                    <input type="number" class="form-control" id="exampleInputIdUsuario" placeholder="ID del usuario" name="usuarios_id" value="<?php echo $inscripcion['usuarios_id']; ?>">
                  </div>                
<!--                   <div class="form-group">
                    <label for="exampleInputUsuarioName">Nombre usuario</label>
                    <input type="text" class="form-control" id="exampleInputUsuarioName" placeholder="Nombre del usuario" name="nombre" value="<?php echo $inscripcion['nombre_usuario']; ?>" disabled>
                  </div> -->
                  <div class="form-group">
                    <label for="exampleInputIdMateria">ID Materia</label>
                    <input type="number" class="form-control" id="exampleInputIdMateria" placeholder="ID de la materia" name="materias_id" value="<?php echo $inscripcion['materias_id']; ?>">
                  </div>                
<!--                   <div class="form-group">
                    <label for="exampleInputmateriaName">Nombre materia</label>
                    <input type="text" class="form-control" id="exampleInputmateriaName" placeholder="Nombre del materia" name="nombre_materia" value="<?php echo $inscripcion['nombre_materia']; ?>" disabled>
                  </div> -->
                </div>              
                <!-- /.card-body -->

                <div class="card-footer">
                   <input type="hidden" name="registro" value="actualizar">

                   <!-- valores originales, necesarios para saber donde editar -->
                   <input type="hidden" name="id_usuario_original" value="<?php echo $usuarios_id_from_queryparam; ?>">
                   <input type="hidden" name="id_materia_original" value="<?php echo $materias_id_from_queryparam; ?>">
                   
                   <button type="submit" class="btn btn-primary">Editar</button>
                   <input type="button" value="Volver" onClick="javascript:history.go(-1)" />
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
