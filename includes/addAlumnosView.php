  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Añadir alumnos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Alumnos</a></li>
              <li class="breadcrumb-item active">Añadir alumnos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Alumnos</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="includes/models/studentsModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Nombre</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Nombre del Alumno" name="nombre">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDni">DNI</label>
                    <input type="number" class="form-control" id="exampleInputDni" placeholder="DNI del Alumno" name="dni">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email del Alumno" name="email">
                  </div>                  
                  <div class="form-group">
                    <label for="exampleInputUsuario">Usuario</label>
                    <input type="text" class="form-control" id="exampleInputUsuario" placeholder="Usuario del Alumno" name="usuario">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword">Contraseña</label>
                    <input type="password" class="form-control" id="exampleInputPassword" placeholder="Ingrese contraseña" name="contrasenia">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputRol">Rol</label>
                    <input type="text" class="form-control" id="exampleInputRol" placeholder="Rol a asignar" name="rol">
                  </div> 
                <!-- /.card-body -->

                <div class="card-footer">
                  <input type="hidden" name="registro" value="nuevo">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
