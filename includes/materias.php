  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Materias</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Materias</a></li>
              <li class="breadcrumb-item active">Lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de materias</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Dia</th>
                    <th>Horario</th>
                    <th>Profesor</th>
                    <th>Cuatrimestre</th>
                    <th>Año</th>
                    <th>Correlatividad</th>
                    <th>Programa</th>
                    <th>Carrera ID</th>
                    <th>Nombre carrera</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php

                      try {
                        # al tener dos tablas con el campo nombre  y no se puede usar algo como 'materias.nombre', uso el alias para lograr el echo correcto
                          $stmt = " SELECT
                                      materias.id,
                                      materias.nombre as materias_nombre,
                                      materias.dia,
                                      materias.horario,
                                      materias.profesor,
                                      materias.cuatrimestre,
                                      materias.anio,
                                      materias.correlatividad,
                                      materias.programa,
                                      materias.carreras_id,
                                      carreras.nombre as carreras_nombre
                                    FROM materias LEFT JOIN carreras
                                    ON materias.carreras_id = carreras.id; ";
                          $resultado = $conn->query($stmt);
                      } catch (Exception $e) {
                          $error = $e->getMessage();
                          echo $error;
                      }
                      while($materias = $resultado->fetch_assoc() ) { ?>
                          <tr>
                              <td><?php echo $materias['id']; ?></td>
                              <td><?php echo $materias['materias_nombre']; ?></td>
                              <td><?php echo $materias['dia']; ?></td>
                              <td><?php echo $materias['horario']; ?></td>
                              <td><?php echo $materias['profesor']; ?></td>
                              <td><?php echo $materias['cuatrimestre']; ?></td>
                              <td><?php echo $materias['anio']; ?></td>
                              <td><?php echo $materias['correlatividad']; ?></td>
                              <td><?php echo $materias['programa']; ?></td>
                              <td><?php echo $materias['carreras_id']; ?></td>
                              <td><?php echo $materias['carreras_nombre']; ?></td>

                              <td>
                                  <a href="editMaterias.php?id=<?php echo $materias['id'] ?>" class="btn bg-orange btn-flat margin">
                                    <i class="fas fa-edit"></i>
                                  </a>
                                  <!-- DATA-TIPO tiene que ser igual a <nombre>Model.php-->
                                  <a href="#" data-id="<?php echo $materias['id']; ?>" data-tipo="materias" class="btn bg-maroon bnt-flat margin borrar_registro">
                                    <i class="fas fa-eraser"></i>
                                  </a>
                              </td>
                          </tr>
                  <?php }  ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Dia</th>
                    <th>Horario</th>
                    <th>Profesor</th>
                    <th>Cuatrimestre</th>
                    <th>Año</th>
                    <th>Correlatividad</th>
                    <th>Programa</th>                    
                    <th>Carrera ID</th>
                    <th>Nombre carrera</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->