<?php 
#include './includes/config/sesiones.php';
include_once './includes/config/bd_conexion.php';

# al no usar id, por tener doble PK, no es necesario
/*
$id = $_GET['id'];
if(!filter_var($id, FILTER_VALIDATE_INT)) {
    die("Error!");
}
*/

include 'includes/header.php';
include 'includes/nav.php';
include 'includes/sidebar.php';

include 'includes/editInscripcionesView.php';

include 'includes/footer.php';
include 'includes/control-sidebar.php';
include 'includes/scripts.php';

?>