<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2020 <a href="https://karkanis.com.ar">Patricio Carcani</a>.</strong> Licencia MIT
</footer>