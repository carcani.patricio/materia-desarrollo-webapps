  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
          <div class="container-fluid">
              <div class="row mb-2">
                  <div class="col-sm-6">
                      <h1 class="m-0">Inscribir alumnos a materias</h1>
                  </div><!-- /.col -->
                  <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                          <li class="breadcrumb-item"><a href="#">Alumnos</a></li>
                          <li class="breadcrumb-item active">Inscribir alumnos</li>
                      </ol>
                  </div><!-- /.col -->
              </div><!-- /.row -->
          </div><!-- /.container-fluid -->

          <!-- /.content-header -->


          <!-- Main content -->
          <section class="content">
              <div class="container-fluid">
                  <div class="row">
                      <!-- left column -->
                      <div class="col-md-12">
                          <!-- general form elements -->
                          <div class="card card-primary">
                              <div class="card-header">
                                  <h3 class="card-title">Alumnos</h3>
                              </div>
                              <!-- /.card-header -->

                              <!-- form start -->
                              <form action="includes/models/inscripcionesModel.php" id="guardar-registro" method="POST">

                                  <div class="card-body">
                                      <div class="form-group">
                                          <label for="alumnos-select">Seleccione un alumno</label>
                                          <select class="form-control selectAlumnos select2" name="alumnos-select"
                                              id="selectAlumnos"
                                              onchange="document.getElementById('alumno-seleccionado').innerHTML=this.value">
                                              <option></option>
                                              <?php
                    try {
                      $sqlqueryAlumnos = "SELECT * FROM usuarios";
                      $resultadoAlumnos = $conn->query($sqlqueryAlumnos);
                      while($alumno=$resultadoAlumnos->fetch_assoc()) { ?>
                                              <option value="<?php echo $alumno['id']; ?>">
                                                  <?php echo $alumno['nombre']; ?>
                                              </option>
                                              <?php }
                    } catch (Exception $e) {
                      echo "Error: " . $e->getMessage();
                    }
                  ?>
                                          </select>
                                          <p><small>Se muestra para debug</small></p>
                                          <p id='alumno-seleccionado'></p>
                                      </div>  
                                  </div>    

                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="materias-select">Seleccione una materia</label>
                                        <select class="form-control selectMaterias select2"
                                            name="materias-select" id="selectMaterias"
                                            onchange="document.getElementById('materia-seleccionada').innerHTML=this.value">
                                            <option></option>
                                            <?php
            try {
            $sqlqueryMaterias = "SELECT * FROM materias";
            $resultadoMaterias = $conn->query($sqlqueryMaterias);
            while($materia=$resultadoMaterias->fetch_assoc()) { ?>
                                            <option value="<?php echo $materia['id']; ?>">
                                                <?php echo $materia['nombre']; ?>
                                            </option>
                                            <?php }
            } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
            }
        ?>
                                        </select>
                                        <p><small>Se muestra para debug</small></p>
                                        <p id='materia-seleccionada'></p>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <input type="hidden" name="registro" value="nuevo">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                              </form>
                          </div>
                          <!-- /.card -->
                      </div>
                      <!--/.col (right) -->
                  </div>
                  <!-- /.row -->
              </div><!-- /.container-fluid -->
          </section>
          <!-- /.content -->
      </div>
    </div>
      <!-- /.content-wrapper -->
      

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
          <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->