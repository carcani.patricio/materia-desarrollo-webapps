  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Carreras</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Carreras</a></li>
              <li class="breadcrumb-item active">Lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de carreras</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Plan de estudio path</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php
                      try {
                          $stmt = "SELECT * FROM carreras";
                          $resultado = $conn->query($stmt);
                      } catch (Exception $e) {
                          $error = $e->getMessage();
                          echo $error;
                      }
                      while($carreras = $resultado->fetch_assoc() ) { ?>
                          <tr>
                              <td><?php echo $carreras['id']; ?></td>
                              <td><?php echo $carreras['nombre']; ?></td>
                              <td><?php echo $carreras['plan_de_estudio_path']; ?></td>
                              <td>
                                  <a href="editCarreras.php?id=<?php echo $carreras['id'] ?>" class="btn bg-orange btn-flat margin">
                                    <i class="fas fa-edit"></i>
                                  </a>
                                  <a href="#" data-id="<?php echo $carreras['id']; ?>" data-tipo="carreras" class="btn bg-maroon bnt-flat margin borrar_registro">
                                    <i class="fas fa-eraser"></i>
                                  </a>
                              </td>
                          </tr>
                  <?php }  ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Plan de estudio path</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->