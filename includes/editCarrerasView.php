  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Editar carrera</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Carreras</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Carreras</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php
                try {
                    $stmt = "SELECT * FROM carreras WHERE id = $id";
                    $resultado = $conn->query($stmt);
                    
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    echo $error;
                }
                $carreras = $resultado->fetch_assoc();
              ?>
              <form action="includes/models/carrerasModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Nombre</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Nombre del Alumno" name="nombre" value="<?php echo $carreras['nombre']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPlanDeEstudioPath">Plan de estudio path</label>
                    <input type="number" class="form-control" id="exampleInputPlanDeEstudioPath" placeholder="Plan de estudio path" name="plan_de_estudio_path" value="<?php echo $carreras['plan_de_estudio_path']; ?>">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                   <input type="hidden" name="registro" value="actualizar">
                   <input type="hidden" name="id_registro" value="<?php echo $id; ?>">
                   <button type="submit" class="btn btn-primary">Guardar</button>
                   <input type="button" value="Volver" onClick="javascript:history.go(-1)" />
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
