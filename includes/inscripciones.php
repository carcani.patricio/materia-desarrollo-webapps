  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Inscripciones</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inscripciones</a></li>
              <li class="breadcrumb-item active">Lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<!--     <aside class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div>

              <form action="guardarSeleccionAlumnoMateria.php" id="seleccionAlumnoMateria" method="POST"> 

                <label for="alumnos-select">Seleccione un alumno</label>
                <select class="form-control selectAlumnos select2" name="alumnos-select" id="selectAlumnos" onchange="document.getElementById('alumno-seleccionado').innerHTML=this.value">
                  <option></option>
                  <?php
                  /*
                    try {
                      $sqlqueryAlumnos = "SELECT * FROM usuarios";
                      $resultadoAlumnos = $conn->query($sqlqueryAlumnos);
                      while($alumno=$resultadoAlumnos->fetch_assoc()) { ?>
                        <option value="<?php echo $alumno['id']; ?>">
                          <?php echo $alumno['nombre']; ?>
                        </option>
                      <?php }
                    } catch (Exception $e) {
                      echo "Error: " . $e->getMessage();
                    }
                    */
                  ?>
                </select>
                <p><small>Se muestra para debug</small></p>
                <p id='alumno-seleccionado'></p>

                  <label for="materias-select">Seleccione una materia</label>
                  <select class="form-control selectMaterias select2" name="materias-select" id="selectMaterias" onchange="document.getElementById('materia-seleccionada').innerHTML=this.value">
                    <option></option>
                    <?php
                    /*
                      try {
                        $sqlqueryMaterias = "SELECT * FROM materias";
                        $resultadoMaterias = $conn->query($sqlqueryMaterias);
                        while($materia=$resultadoMaterias->fetch_assoc()) { ?>
                          <option value="<?php echo $materia['id']; ?>">
                            <?php echo $materia['nombre']; ?>
                          </option>
                        <?php }
                      } catch (Exception $e) {
                        echo "Error: " . $e->getMessage();
                      }
                      */
                    ?>
                  </select>
                  <p><small>Se muestra para debug</small></p>
                  <p id='materia-seleccionada'></p>

                  <br>

                  <script type="text/javascript">
                    function cambiarHref() {

                      var url = "editInscripciones.php?usuarios_id=" + document.getElementById("alumno-seleccionado").innerHTML + "&materias_id=" + document.getElementById("materia-seleccionada").innerHTML;
                      
                      guardarAlumnoMateria.setAttribute("href", url);
                      return false;

                    }
                  </script>                  
                  
                  <a id="guardarAlumnoMateria" class="btn bg-orange btn-flat margin" onclick="cambiarHref()">
                  
                   <a href="editInscripciones.php" class="btn bg-orange btn-flat margin"> -->
                  <!-- <a href="editInscripciones.php?usuarios_id=<script>
function myFunction() {
  document.getElementById('materia-seleccionada').innerHTML = 'Paragraph changed!';
}
</script>&materias_id=<?php /*echo $materia['materias_id']*/ ?>" class="btn bg-orange btn-flat margin"> -->
                   <!-- <i class="fas fa-save"></i> Guardar
                  </a>
                </form>
            </div>
          </div>
        </div>
      </div>
    </aside>

    <br><br> -->
<div>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Lista de inscripciones</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Usuario ID</th>
                    <th>Usuario</th>
                    <th>Materia ID</th>
                    <th>Materia</th>
                    <!-- habra q agregar campos? -->
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php
                      try {
                          $stmt = "SELECT usuarios_id,
                                   usuarios.nombre as nombre_usuario,
                                   materias_id,
                                   materias.nombre as nombre_materia
                                   FROM usuarios_has_materias
                                   INNER JOIN materias ON usuarios_has_materias.materias_id=materias.id
                                   INNER JOIN usuarios ON usuarios_has_materias.usuarios_id=usuarios.id
                                   ORDER BY usuarios_id
                                   ";
                          $resultado = $conn->query($stmt);
                      } catch (Exception $e) {
                          $error = $e->getMessage();
                          echo $error;
                      }
                      while($admin = $resultado->fetch_assoc() ) { ?>
                          <tr>
                              <td><?php echo $admin['usuarios_id']; ?></td>
                              <td><?php echo $admin['nombre_usuario']; ?></td>
                              <td><?php echo $admin['materias_id']; ?></td>
                              <td><?php echo $admin['nombre_materia']; ?></td>
                              <td>
                              <!-- funciona con 2 -->
                                  <a href="editInscripciones.php?usuarios_id=<?php echo $admin['usuarios_id'] ?>&materias_id=<?php echo $admin['materias_id'] ?>" class="btn bg-orange btn-flat margin">
                                    <i class="fas fa-edit"></i>
                                  </a>
                                  <a href="#" data-alumno-id="<?php echo $admin['usuarios_id']; ?>" data-materia-id="<?php echo $admin['materias_id']; ?>" data-tipo="inscripciones" class="btn bg-maroon bnt-flat margin borrar_registro_inscripciones">
                                    <i class="fas fa-eraser"></i>
                                  </a>
                              </td>
                          </tr>
                  <?php }  ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Usuario ID</th>
                    <th>Usuario</th>
                    <th>Materia ID</th>
                    <th>Materia</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->