  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Editar usuario</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Usuarios</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php
                  try {
                    $stmt = "SELECT * FROM usuarios WHERE id = $id";
                    $resultado = $conn->query($stmt);
                    
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    echo $error;
                }
                $student = $resultado->fetch_assoc();
                  
              ?>
              <form action="includes/models/studentsModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Nombre</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Nombre del Alumno" name="nombre" value="<?php echo $student['nombre']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputLastname">DNI</label>
                    <input type="number" class="form-control" id="exampleInputDni" placeholder="DNI del Alumno" name="dni" value="<?php echo $student['dni']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email del Alumno" name="email" value="<?php echo $student['email']; ?>">
                  </div>                  
                  <div class="form-group">
                    <label for="exampleInputUsuario">Usuario</label>
                    <input type="text" class="form-control" id="exampleInputUsuario" placeholder="Usuario del Alumno" name="usuario" value="<?php echo $student['usuario']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword">Contraseña</label>
                    <input type="password" class="form-control" id="exampleInputPassword" placeholder="Ingrese contraseña" name="contrasenia" value="<?php echo $student['contrasenia']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputRol">Rol</label>
                    <input type="text" class="form-control" id="exampleInputRol" placeholder="Rol a asignar" name="rol" value="<?php echo $student['rol']; ?>">
                  </div>                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                   <input type="hidden" name="registro" value="actualizar">
                   <input type="hidden" name="id_registro" value="<?php echo $id; ?>">
                   <button type="submit" class="btn btn-primary">Guardar</button>
                   <input type="button" value="Volver" onClick="javascript:history.go(-1)" />
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
