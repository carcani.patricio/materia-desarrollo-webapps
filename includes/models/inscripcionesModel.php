<?php

// Incluimos la conexion con la base de datos
include_once '../config/bd_conexion.php';

// Evaluamos con IF si en la variable post registro el valor es nuevo en caso de true se ejecuta el script que contiene
if($_POST['registro'] == 'nuevo'){

    // Capturamos los datos recibidos por post que nos envia el formulario
    $usuarios_id = $_POST['alumnos-select'];
    $materias_id = $_POST['materias-select'];

    $stmt = " SELECT * FROM usuarios_has_materias WHERE usuarios_id ='$usuarios_id' AND materias_id = '$materias_id' ";
    
    $user_very = $conn->query($stmt);

    if (mysqli_num_rows($user_very)==0) {
        # code...

        // con la funcion try ejecutamos el codigo en modo debug y capturamos los errores si se producen
        try {
            // creamos el obvjeto STMT y le asignamos el Objeto $conn que es el objeto creado en la conexion con la base de datos, utilizamos el metodo "prepare" de la clase mysqli para preparar la query sql
            $stmt = $conn->prepare(" INSERT INTO usuarios_has_materias(usuarios_id, materias_id) VALUES (?, ?) ");            
            // HARDOCEADO, FUNCIONA Y LUEGO ES LISTADA
            // $stmt = $conn->prepare(" INSERT INTO usuarios_has_materias (usuarios_id,dni,email,usuario,contrasenia,rol) VALUES ('hardoceado', '3', 'mail@df.com', 'user', 'a', 'Admin') ");
            // con el metodo bind_param asignamos los valores y los tipos de datos correspondientes
            $stmt->bind_param("ii", $usuarios_id, $materias_id);
            // ejecutamos la query con "execute"
            $stmt->execute();
            // en la variable "id_insertado" capturamos el ID que nos devuelve mysql
            $id_insertado = $stmt->insert_id;
            // en esta condicion consultamos si mysql nos devuelve alguna fila afectada de ser verdadero quiere decir que se ejecuto la query y se guardaron los datos.
            if($stmt->affected_rows) {
                // generamos el array respuesta este va ser en formato json para devolverselo a ajax
                $respuesta = array(
                    'respuesta' => 'exito',
                    'id_registro' => $id_insertado,
                );
                // si lo anterior no se cumple el array va a contener el mensaje error para generar el cartel correspondiente.
            } else {
                $respuesta = array(
                    'respuesta' => 'error'
                );
            }
            // cerramos las conexiones correspondiente
            $stmt->close();
            $conn->close(); 
        }
        // con catch capturamos las exceciones $e es la excecion que devuelve el try que se ejecuto antes, si existe en el array respuesta pasamos el mensaje que contiene para mitigar el error.
        catch (Exception $e) {
            $respuesta = array(
                'respuesta' => $e->getMessage()
            );
        }

    } else {
        $respuesta = array(
            'respuesta' => 'error-users'
        );
    }
    // con "die" frenamos la ejecucion y enviamos el array respuesta con "json_encode" le retornamos a ajax la respuesta en json.
    die(json_encode($respuesta));
}

//***** UPDATE *****
// este script es exactamente igual que el anterior pero cambia la query ya es para actualizar, en este capturamos el id del campo que vamos a actualizar, este es enviado atravez del formulario. 
if($_POST['registro'] == 'actualizar'){

    $usuarios_id = $_POST['usuarios_id'];
    $materias_id = $_POST['materias_id'];

    $usuarios_id_original = $_POST['id_usuario_original'];
    $materias_id_original = $_POST['id_materia_original'];
 
    try {
// el error los logs de apache marcan que esta en la parte de bind_param, pero la dev console dice que envia bien los datos, sera q no los recibe/toma?
        $stmt = $conn->prepare(" UPDATE usuarios_has_materias SET usuarios_id=?, materias_id=? WHERE usuarios_id='$usuarios_id_original' AND materias_id='$materias_id_original' ");
        $stmt->bind_param("ii", $usuarios_id, $materias_id);
        $stmt->execute();

        if($stmt->affected_rows) {
            $respuesta = array(
                'respuesta' => 'exito',
                'usuarios_id' => $usuarios_id,
                'materias_id' => $materias_id
            );
        } else {
            $respuesta = array(
                'respuesta' => 'error'
            );
        }

        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        $respuesta = array(
            'respuesta' => $e->getMessage()
        );
    }

    die(json_encode($respuesta));
    

}

//***** DELETE *****
// en este script eliminamos un registro, para eso solo necesitamos capturar el id y en la query pasarlo al "where" para que nos borre el campo correcto unicamente, de no espeificar el where eliminara todos los campos.
if($_POST['registro'] == 'eliminar'){

    $usuarios_id = $_POST['usuarios_id'];
    $materias_id = $_POST['materias_id'];

    try {
        $stmt = $conn->prepare(' DELETE FROM usuarios_has_materias WHERE usuarios_id=? AND materias_id=? ');
        $stmt->bind_param("ii", $usuarios_id, $materias_id);
        $stmt->execute();
        if($stmt->affected_rows) {
            $respuesta = array(
                'respuesta' => 'exito',
                // esto se envia al success fx de ajax
                'usuarios_id_eliminado' => $usuarios_id,
                'materias_id_eliminado' => $materias_id
            );
        } else {
            $respuesta = array(
                'respuesta' => 'error'
            );
        }
    } catch (Exception $e) {
        $respuesta = array(
            'respuesta' => $e->getMessage()
        );
    }
    die(json_encode($respuesta));
}
?>