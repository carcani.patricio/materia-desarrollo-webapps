  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Añadir materia</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Materias</a></li>
              <li class="breadcrumb-item active">Añadir materia</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Materias</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="includes/models/materiasModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Nombre</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Nombre de la materia" name="nombre">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDia">Dia</label>
                    <input type="number" class="form-control" id="exampleInputDia" placeholder="Dia de la materia" name="dia">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputHorario">Horario</label>
                    <input type="text" class="form-control" id="exampleInputHorario" placeholder="Horario de la materia" name="horario">
                  </div>                  
                  <div class="form-group">
                    <label for="exampleInputProfesor">Profesor</label>
                    <input type="text" class="form-control" id="exampleInputProfesor" placeholder="Profesor de la materia" name="profesor">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputCuatrimestre">Cuatrimestre</label>
                    <input type="number" class="form-control" id="exampleInputCuatrimestre" placeholder="Ingrese cuatrimestre" name="cuatrimestre">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputAnio">Año</label>
                    <input type="number" class="form-control" id="exampleInputAnio" placeholder="Año a asignar" name="anio">
                  </div>
                <div class="form-group">
                    <label for="exampleInputCorrelatividad">Correlatividad</label>
                    <input type="text" class="form-control" id="exampleInputCorrelatividad" placeholder="Correlatividad de la materia" name="correlatividad">
                  </div>                  
                  <div class="form-group">
                    <label for="exampleInputPrograma">Programa</label>
                    <input type="text" class="form-control" id="exampleInputPrograma" placeholder="Programa de la materia" name="programa">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputCarreras">Carreras</label>
                    <input type="number" class="form-control" id="exampleInputCarreras" placeholder="Ingrese carrera" name="carreras_id">
                  </div>  
                </div>              
                <!-- /.card-body -->

                <div class="card-footer">
                  <input type="hidden" name="registro" value="nuevo">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
