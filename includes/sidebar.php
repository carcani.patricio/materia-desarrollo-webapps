  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="./index3.html" class="brand-link elevation-4">
      <img src="img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">IFTS Nº4</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Pato Carcani</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">

            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Alumnos
              </p>
            </a>

              <ul class="treeview-menu">
                <li class="nav-item">
                  <a href="addAlumnos.php">Añadir alumnos</a>
                </li>
                <li class="nav-item">
                  <a href="listarAlumnos.php">Listar alumnos</a>
                </li>
              </ul>

          </li>

          <li class="nav-item">

            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Materias
              </p>
            </a>

              <ul class="treeview-menu">
                <li class="nav-item">
                  <a href="addMaterias.php">Añadir materias</a>
                </li>
                <li class="nav-item">
                  <a href="listarMaterias.php">Listar materias</a>
                </li>
              </ul>

          </li>          

          <li class="nav-item">

            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Carreras
              </p>
            </a>

              <ul class="treeview-menu">
                <li class="nav-item">
                  <a href="addCarreras.php">Añadir carreras</a>
                </li>
                <li class="nav-item">
                  <a href="listarCarreras.php">Listar carreras</a>
                </li>
              </ul>

          </li>    

          <li class="nav-item">

            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Inscripciones
              </p>
            </a>

              <ul class="treeview-menu">
                <li class="nav-item">
                  <a href="addInscripciones.php">Añadir inscripciones</a>
                </li>
                <li class="nav-item">
                  <a href="listarInscripciones.php">Listar inscripciones</a>
                </li>
              </ul>

          </li>  

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>